package com.example.fragments.fragment

import android.os.Bundle
import android.widget.RatingBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentContainer
import com.example.fragments.R

class RatingFragment : Fragment(R.layout.fragment_rating) {
    private lateinit var ratingBar: RatingBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.fragment_rating)

        ratingBar.rating = 2.5f
        ratingBar.stepSize = .5f

        ratingBar.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
            Toast.makeText(context, "Rating: $rating. Thank You!", Toast.LENGTH_SHORT).show()
        }

    }




}






